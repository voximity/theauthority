﻿using Discord;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheAuthority.Discord
{
    class Bot
    {
		public List<Shard> Shards = new List<Shard>();
		public Log Log = new Log("bot");

		public Bot(int shards) {
			for (int i = 0; i < shards; i++) {
				Shard shard = new Shard(i, shards);
				Shards.Add(shard);
			}
			Log.Out("Instantiated shards.");
		}

		public async Task Connect(string token) {
			bool avatarChanged = false;
			foreach (Shard shard in Shards) {
				Log.Out($"Attempting to connect shard {Shards.IndexOf(shard)}/{Shards.Count}");
				await shard.Connect(token);
				await Task.Delay(2000);
			}
			Log.Out("Completed connecting shards.");
		}
    }
}
