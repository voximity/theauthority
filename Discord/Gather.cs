﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAuthority.Discord
{
    class Gather
    {
		public static SocketTextChannel Channel(SocketGuild guild, string path) {
			if (!path.Contains("/")) {
				return guild.TextChannels.FirstOrDefault(c => c.Name == path.ToLower());
			} else {
				string categoryName = path.Substring(0, path.IndexOf("/")).ToLower();
				string channelName = path.Substring(path.IndexOf("/") + 1).ToLower();
				return (SocketTextChannel)guild.CategoryChannels.FirstOrDefault(c => c.Name.ToLower() == categoryName).Channels.FirstOrDefault(c => c.Name == channelName);
			}
		}

		public static SocketRole Role(SocketGuild guild, string name) {
			return guild.Roles.FirstOrDefault(r => r.Name.ToLower() == name.ToLower());
		}
    }
}
