﻿using Discord;
using Discord.WebSocket;
using SharpNoise;
using SixLabors.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TheAuthority.Game;
using TheAuthority.Game.Commands;

namespace TheAuthority.Discord
{
    class Shard
    {
		public DiscordSocketClient Client;

		public int ShardIndex;
		public int ShardCount;

		public Log Log;

		private string Token;

		public Shard(int index, int count) {
			ShardIndex = index;
			ShardCount = count;

			Log = new Log($"shard {ShardIndex}");

			Client = new DiscordSocketClient(new DiscordSocketConfig {
				ShardId = ShardIndex,
				TotalShards = ShardCount
			});
		}

		public async Task Connect(string token) {
			Token = token;

			InitializeClient();

			await Client.LoginAsync(TokenType.Bot, token);
			await Client.StartAsync();

			Log.Out("Started shard client.");
		}

		public void InitializeClient() {
			Random random = new Random();

			Client.Ready += async () => {
				Log.Out("Received ready payload.");
			};

			Client.MessageReceived += (m) => {
				Log.Out($"{m.Author.Username}: {m.Content}");

				ThreadStart start = async () => {
					await Command.ParseCommand(this, (SocketUserMessage)m);
				};

				Thread thread = new Thread(start);
				thread.Start();
				return null;
			};
		}
    }
}
