﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using TheAuthority.Discord;

namespace TheAuthority
{
	class Config {
		public string Token;
	}

    class Program
    {
		public static Bot Bot;
		public static Config Config;

		static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

		static async Task MainAsync() {
			string rawConfig = await File.ReadAllTextAsync("config.json");
			Config = JsonConvert.DeserializeObject<Config>(rawConfig);
			Database.Init();

			Bot = new Bot(1);

			await Bot.Connect(Config.Token);

			await Task.Delay(-1);
		}
    }
}
