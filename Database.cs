﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheAuthority.Game;

namespace TheAuthority
{
	class DbItem {
		[BsonId]
		public ulong Id;
	}

	class DbCollection<T> where T : DbItem {
		private IMongoCollection<T> collection;

		public DbCollection(IMongoDatabase db, string name) {
			collection = db.GetCollection<T>(name);
		}

		public Task AddAsync(T item) => collection.InsertOneAsync(item);

		public async Task<bool> ExistsAsync(ulong id) => await collection.CountDocumentsAsync(
			Builders<T>.Filter.Eq(i => i.Id, id)
		) > 0;

		public async Task<T> GetAsync(ulong id) => await (await collection.FindAsync(
			Builders<T>.Filter.Eq(i => i.Id, id)
		)).FirstAsync();

		public Task<T> GetAsync(T item) => GetAsync(item.Id);

		public Task UpdateAsync(T item) => collection.ReplaceOneAsync(
			Builders<T>.Filter.Eq(i => i.Id, item.Id),
			item
		);
	}

    static class Database
    {
		public static MongoClient Client;
		public static IMongoDatabase Db;

		public static DbCollection<Nation> Nations;

		public static void Init() {
			Client = new MongoClient("mongodb://localhost:27017");
			Db = Client.GetDatabase("theauthority");

			Nations = new DbCollection<Nation>(Db, "nations");
		}
		
    }
}
