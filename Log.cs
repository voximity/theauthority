﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheAuthority
{
	enum Severity {
		Info,
		Warn,
		Error
	}

    class Log
    {
		public static Log Global = new Log("global");

		public string Name;

		public Log(string name) {
			Name = name;
		}

		public void Out(object content, Severity severity = Severity.Info) {
			Console.ResetColor();
			switch (severity) {
				case Severity.Info:
					Console.ForegroundColor = ConsoleColor.Cyan;
					break;
				case Severity.Warn:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				case Severity.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
			}
			Console.Write($"[{Name}] ");
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine(content.ToString());
			Console.ResetColor();
		}
    }
}
