﻿using Discord.WebSocket;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheAuthority.Game.Work;

namespace TheAuthority.Game
{

    class Citizen : NationObject {
		[BsonIgnore]
		public string LocationRole => $"In {LocationName}";

		[BsonElement("age")]
		public int Age;

		[BsonElement("id")]
		public ulong DiscordId;

		[BsonElement("name")]
		public string Name;

		[BsonElement("location")]
		public string LocationName;

		[BsonElement("balance")]
		public uint Balance = 0;

		public void TravelTo(City destination) {
			LocationName = destination.Name;
		}

		public List<CompanyJob> GetJobs(Nation nation) {
			List<CompanyJob> jobs = new List<CompanyJob>();
			foreach (Company company in nation.Companies) {
				foreach (Job job in company.Jobs) {
					if (job is SingleJob) {
						if ((job as SingleJob).OccupantId == DiscordId)
							jobs.Add(new CompanyJob(company, job));
					} else if (job is ManyJob) {
						if ((job as ManyJob).Occupants.Contains(DiscordId))
							jobs.Add(new CompanyJob(company, job));
					}
				}
			}
			return jobs;
		}

		public async Task<List<CompanyJob>> GetJobsAsync() => GetJobs(await GetNationAsync());
    }
}
