﻿using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;
using System.IO;
using System;
using System.Collections.Generic;
using System.Text;
using SharpNoise.Utilities.Imaging;
using System.Drawing.Imaging;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Drawing;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.Shapes;
using SixLabors.ImageSharp.Processing.Text;
using System.Linq;
using Discord.WebSocket;
using System.Threading.Tasks;
using Discord;
using TheAuthority.Discord;
using MongoDB.Bson.Serialization.Attributes;
using TheAuthority.Game.Work;
//using System.Drawing;

namespace TheAuthority.Game
{
	class NationObject {
		[BsonElement("nation")]
		public ulong NationId;
		
		public Task<Nation> GetNationAsync() => Database.Nations.GetAsync(NationId);
	}

	class Nation : DbItem {
		private static Font cityRenderFont = SystemFonts.CreateFont("Arial", 14);

		[BsonElement("name")]
		public string Name;

		[BsonElement("cities")]
		public List<City> Cities;

		[BsonElement("citizens")]
		public List<Citizen> Citizens;

		[BsonElement("companies")]
		public List<Company> Companies;
		
		[BsonElement("capital")]
		public string CapitalName;
		[BsonIgnore]
		public City Capital => Cities.Find(c => c.Name == CapitalName);

		[BsonElement("government")]
		public Company Government = new Company {
			Name = "Government",
			Jobs = new Jobs {
				new SingleJob("President", 3.5),
				new SingleJob("Vice President", 3)
			}
		};

		[BsonElement("seed")]
		public int Seed => Island.GetNameSeed(Name);
		[BsonIgnore]
		public NoiseMap Map => Island.GenerateMap(Seed, 600, 600, 2, 2);

		public Nation(string name) {
			Name = name;
			Cities = new List<City>();
			Citizens = new List<Citizen>();
			Companies = new List<Company> {
				Government
			};
		}

		public void RenderCity(Image<Rgba32> image, City city, Rgba32? color = null) {
			image.Mutate(ctx => ctx
				.Fill(color ?? Rgba32.White, new EllipsePolygon(city.Location.Coordinates, 5))
				.DrawText(new TextGraphicsOptions(true) {
					HorizontalAlignment = HorizontalAlignment.Center,
					VerticalAlignment = VerticalAlignment.Center
				},
				city.Name, cityRenderFont, color ?? Rgba32.White, new SixLabors.Primitives.PointF(city.Location.X, city.Location.Y + 14)));
		}

		public Image<Rgba32> Render() {
			Image<Rgba32> heightmap = Island.RenderNoiseMap(Map);

			foreach(City city in Cities) {
				RenderCity(heightmap, city);
			}

			return heightmap;
		}

		public string RenderAndSave() {
			return Island.SaveImage(Render(), $"nation-{Name.ChannelName()}");
		}

		public async Task<City> AddCityAndChannel(City city, ICategoryChannel nationCategory) {
			city.NationId = Id;
			Cities.Add(city);

			IRole cityRole = await nationCategory.Guild.CreateRoleAsync($"In {city.Name}");
			ITextChannel cityChannel = await nationCategory.Guild.CreateTextChannelAsync(city.Name.ChannelName(), c => c.CategoryId = nationCategory.Id);
			await cityChannel.AddPermissionOverwriteAsync(nationCategory.Guild.EveryoneRole, new OverwritePermissions(viewChannel: PermValue.Deny));
			await cityChannel.AddPermissionOverwriteAsync(cityRole, new OverwritePermissions(viewChannel: PermValue.Allow));

			return city;
		}

		public Citizen GetCitizen(SocketGuildUser user) {
			if (!Citizens.Any(c => c.DiscordId == user.Id))
				throw new NoCitizenException();
			return Citizens.FirstOrDefault(c => c.DiscordId == user.Id);
		}

		public async Task<Citizen> AddCitizen(SocketGuildUser user, string name = null) {
			if (Citizens.Any(c => c.DiscordId == user.Id))
				throw new CitizenExistsException();

			Citizen citizen = new Citizen {
				DiscordId = user.Id,
				LocationName = CapitalName,
				NationId = Id,
				Name = name ?? user.Username
			};

			await user.AddRoleAsync(Gather.Role(user.Guild, "Citizen"));
			await user.AddRoleAsync(Gather.Role(user.Guild, citizen.LocationRole));

			try {
				if (name != null)
					await user.ModifyAsync(u => u.Nickname = name);
			} catch { }

			Citizens.Add(citizen);
			return citizen;
		}

		public static async Task<Nation> Establish(string name, SocketGuildUser founder, string founderName, City capital) {
			Nation nation = new Nation(name);
			nation.Id = founder.Guild.Id;
			nation.CapitalName = capital.Name;

			SocketGuild guild = founder.Guild;

			IRole citizenRole = await guild.CreateRoleAsync("Citizen", isHoisted: true);

			ICategoryChannel nationCategory = await guild.CreateCategoryChannelAsync("nation");
			await nationCategory.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(sendMessages: PermValue.Deny));
			await nationCategory.AddPermissionOverwriteAsync(citizenRole, new OverwritePermissions(sendMessages: PermValue.Allow));

			ITextChannel globalNationChannel = await guild.CreateTextChannelAsync("global", c => c.CategoryId = nationCategory.Id);
			
			await nation.AddCityAndChannel(capital, nationCategory);

			string mapFile = nation.RenderAndSave();
			await globalNationChannel.SendFileAsync(mapFile, $"Established nation **{name}**.");
			File.Delete(mapFile);

			Citizen president = await nation.AddCitizen(founder, founderName);
			nation.Government.GiveCitizenJob(president, "President");

			await Database.Nations.AddAsync(nation);

			return nation;
		}
    }
}
