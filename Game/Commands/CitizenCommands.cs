﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheAuthority.Discord;
using TheAuthority.Game.Work;

namespace TheAuthority.Game.Commands
{
	class CommandBalance : Command {
		public override string Name => "Balance";
		public override string Description => "View your balance.";
		public override string[] CallAliases => new string[] { "balance", "money", "my balance" };

		public override async Task Execute(CommandContext context) {
			Nation nation = await context.GetNation();
			Citizen citizen = nation.GetCitizen(context.User);

			await context.Reply($"Your balance is {citizen.Balance.Money().Bold()}.");
		}
	}

	class CommandJobs : Command {
		public override string Name => "Jobs";
		public override string Description => "View your job(s).";
		public override string[] CallAliases => new string[] { "jobs", "my jobs", "view my jobs", "show my jobs" };

		public override async Task Execute(CommandContext context) {
			Nation nation = await context.GetNation();
			Citizen citizen = nation.GetCitizen(context.User);
			List<CompanyJob> jobs = citizen.GetJobs(nation);

			if (jobs.Count < 1)
				await context.Reply("You have no jobs.");
			else if (jobs.Count == 1) {
				CompanyJob job = jobs.First();
				await context.Reply($"You are currently {job.Job.Name.Bold()} at {job.Company.Name.Bold()}.");
			} else {
				await context.ReplyEmbed("Jobs", jobs.Aggregate("", (p, c) => p += $"- {c.Job.Name.Bold()} at {c.Company.Name.Bold()}\n"));
			}
		}
	}
}
