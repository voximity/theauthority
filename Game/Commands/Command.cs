﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TheAuthority.Discord;

namespace TheAuthority.Game.Commands
{
	class CommandContext {
		/// <summary>
		/// The content of the message.
		/// </summary>
		public string Content;

		/// <summary>
		/// The resolved content, where mentions are not formatted and appear as they would in Discord.
		/// </summary>
		public string Resolved;

		/// <summary>
		/// The message object representing this command execution.
		/// </summary>
		public SocketUserMessage Message;

		/// <summary>
		/// The server member representing this command execution.
		/// </summary>
		public SocketGuildUser User;

		/// <summary>
		/// The guild channel representing this command execution. Don't send messages from here. Use TextChannel instead.
		/// </summary>
		public SocketGuildChannel Channel;

		/// <summary>
		/// The guild representing this command execution. Contains a lot of useful information.
		/// </summary>
		public SocketGuild Guild;

		/// <summary>
		/// The text channel representing this command execution. Use this for sending messages.
		/// </summary>
		public SocketTextChannel TextChannel;

		/// <summary>
		/// A dictionary of arguments passed.
		/// </summary>
		public Dictionary<string, string> Args = new Dictionary<string, string>();


		
		public async Task<Nation> GetNation() {
			if (await Database.Nations.ExistsAsync(Guild.Id))
				return await Database.Nations.GetAsync(Guild.Id);
			throw new NoNationExistsException();
		}
		public async Task<Nation> GetNationSafe() {
			if (await Database.Nations.ExistsAsync(Guild.Id))
				return await Database.Nations.GetAsync(Guild.Id);
			return null;
		}

		/// <summary>
		/// Reply with a specified message. Optionally, pass a severity value showing how severe the message is.
		/// </summary>
		/// <param name="message">A string containing the content of the message.</param>
		/// <param name="severity">The LogSeverity enum, denoting how severe the message is. Choose between Info, Warn, and Error.</param>
		/// <returns>Use await to call this.</returns>
		public async Task Reply(string message, Severity severity = Severity.Info) {
			string m = "";
			if (severity == Severity.Warn)
				m += ":warning: ";
			else if (severity == Severity.Error)
				m += ":x: ";
			m += message;
			await TextChannel.SendMessageAsync(m);
		}

		/// <summary>
		/// Reply with a very specific embed. You likely won't need to use this endpoint.
		/// </summary>
		/// <param name="title">The content of the title of this embed.</param>
		/// <param name="description">The description of the embed.</param>
		/// <param name="fields">A list of fields to include. Optional.</param>
		/// <param name="color">A color. Optional.</param>
		/// <param name="imagePath">A path to an image to include.</param>
		/// <returns>Use await to call this.</returns>
		public async Task ReplySpecificEmbed(string title, string description, EmbedFieldBuilder[] fields = null, Color? color = null, string imagePath = null) {
			EmbedBuilder embed = new EmbedBuilder()
				.WithTitle(title)
				.WithDescription(description);

			if (color != null)
				embed.WithColor(color.Value);
			if (fields != null)
				foreach (EmbedFieldBuilder field in fields)
					embed.AddField(field);

			if (imagePath == null)
				await TextChannel.SendMessageAsync(embed: embed.Build());
			else {
				embed.WithImageUrl($"attachment://{imagePath}");
				await TextChannel.SendFileAsync(imagePath, "", embed: embed.Build());
			}
		}

		/// <summary>
		/// Reply with a less specific embed. Uses a preset color based on the passed LogSeverity.
		/// </summary>
		/// <param name="title">The content of the title of this embed.</param>
		/// <param name="description">The description of the embed.</param>
		/// <param name="severity">The severity of the message. Determines the color.</param>
		/// <param name="imagePath">A path to an image to include.</param>
		/// <param name="fields">A list of fields to include. Optional.</param>
		/// <returns></returns>
		public Task ReplyEmbed(string title, string description, Severity severity = Severity.Info, string imagePath = null, EmbedFieldBuilder[] fields = null) {
			if (severity == Severity.Info)
				return ReplySpecificEmbed(title, description, fields, null, imagePath);
			else if (severity == Severity.Warn)
				return ReplySpecificEmbed(title, description, fields, Color.Orange, imagePath);
			else if (severity == Severity.Error)
				return ReplySpecificEmbed(title, description, fields, Color.Red, imagePath);
			else
				return null;
		}

		/// <summary>
		/// Construct a CommandContext argument from a SocketUserMessage.
		/// </summary>
		/// <param name="message">The message to be constructed from. MUST have been sent from a guild.</param>
		/// <returns>A context.</returns>
		public static CommandContext FromMessage(SocketUserMessage message) =>
			new CommandContext {
				Content = message.Content,
				Resolved = message.Resolve(),

				Message = message,
				User = (SocketGuildUser)message.Author,
				Channel = (SocketGuildChannel)message.Channel,
				Guild = ((SocketGuildChannel)message.Channel).Guild,
				TextChannel = (SocketTextChannel)message.Channel
			};
	}

    abstract class Command
    {
		public static Command[] Commands = new Command[] {
			new CommandRandomIsland(),
			new CommandIslandSeed(),

			new CommandEstablish(),
			new CommandNationMap(),
			new CommandJoinNation(),

			new CommandBalance(),
			new CommandJobs()
		};

		public static async Task ParseCommand(Shard shard, SocketUserMessage message) {
			SocketGuild guild = ((SocketGuildChannel)message.Channel).Guild;

			string prefix =  guild.CurrentUser.Nickname != null ? (guild.CurrentUser.Mention) : (guild.CurrentUser.Mention.Replace("!", ""));
			string content = message.Content;
			string resolved = message.Resolve();

			// The message doesn't start with the prefix.
			if (!content.StartsWith(prefix)) return;

			// The command content is everything after the prefix.
			string commandContent = content.Substring(prefix.Length + 1);

			Dictionary<string, string> args = new Dictionary<string, string>();

			Command matchedCommand = null;
			foreach (Command command in Commands) {
				foreach (string callAlias in command.CallAliases) {
					string alias = "^" + Regex.Replace(callAlias, "<(.*?)>", m => $"(?{m.Groups.First().Value}.*?)") + "$";
					MatchCollection matches = Regex.Matches(commandContent, alias, RegexOptions.None);
					if (!matches.Any()) continue;
					Match match = matches.First();

					foreach (Group group in match.Groups) {
						if (int.TryParse(group.Name, out int _)) continue;
						args[group.Name] = group.Value;
					}
					matchedCommand = command;
					break;
				}
			}

			if (matchedCommand == null) return;

			CommandContext context = CommandContext.FromMessage(message);
			context.Args = args;

			try {
				await matchedCommand.Execute(context);
			} catch(Exception e) {
				await context.ReplyEmbed("Unknown error", "An unknown error has occured.", Severity.Error, fields: new EmbedFieldBuilder[] {
					new EmbedFieldBuilder().WithName(e.GetType().Name).WithValue(e.Message)
				});
				shard.Log.Out($"{e.GetType().Name}: {e.Message}\n{e.StackTrace}");
			}

		}

		public abstract string Name { get; }
		public abstract string Description { get; }
		public abstract string[] CallAliases { get; }

		public abstract Task Execute(CommandContext context);
	}
}
