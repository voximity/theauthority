﻿using SixLabors.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TheAuthority.Game.Commands
{
	class CommandEstablish : Command {
		public override string Name => "Establish Nation";
		public override string Description => "Establish a new nation.";
		public override string[] CallAliases => new string[] { "establish nation <name> with capital <capital> as <president>" };

		public override async Task Execute(CommandContext context) {
			if (await context.GetNationSafe() != null)
				throw new NationExistsException();

			string nationName = context.Args["name"];
			string capitalName = context.Args["capital"];
			string presidentName = context.Args["president"];

			City capital = new City {
				Location = new GeographicLocation(300, 300),
				Name = capitalName
			};

			await Nation.Establish(nationName, context.User, presidentName, capital);
		}
	}

	class CommandJoinNation : Command {
		public override string Name => "Join Nation";
		public override string Description => "Establishes yourself as an official citizen of this nation.";
		public override string[] CallAliases => new string[] { "join nation as <name>", "join as <name>" };

		public override async Task Execute(CommandContext context) {
			Nation nation = await context.GetNation();
			await nation.AddCitizen(context.User, context.Args["name"]);
			await Database.Nations.UpdateAsync(nation);
		}
	}

	class CommandNationMap : Command {
		public override string Name => "Show Nation Map";
		public override string Description => "Shows the nation's map.";
		public override string[] CallAliases => new string[] { "show map", "show nation map", "show island", "show nation island", "nation map", "map" };

		public override async Task Execute(CommandContext context) {
			Nation nation = await context.GetNation();

			string file = nation.RenderAndSave();
			Log.Global.Out(file);
			await context.ReplyEmbed("Nation Map", "Below is a map of this nation.", imagePath: file);
			File.Delete(file);
		}
	}
}
