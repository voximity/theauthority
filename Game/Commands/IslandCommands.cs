﻿using SharpNoise;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TheAuthority.Discord;

namespace TheAuthority.Game.Commands
{
    class CommandRandomIsland : Command
    {
		public static Random Random = new Random();

		public override string Name => "Random Island";
		public override string Description => "Generates and displays a random island.";
		public override string[] CallAliases => new string[] { "random island", "make random island", "generate island" };

		public override async Task Execute(CommandContext context) {
			int seed = Random.Next(int.MinValue, int.MaxValue);

			NoiseMap map = Island.GenerateMap(seed, 600, 600, 2, 2);
			string filename = Island.RenderAndSave(map);

			await context.ReplyEmbed("Random island", $"A random island has been generated with seed {seed.ToString().Bold()}.", imagePath: filename);
			File.Delete(filename);
		}
	}

	class CommandIslandSeed : Command {
		public static Random Random = new Random();

		public override string Name => "Seed Island";
		public override string Description => "Generates and displays an island based on the given seed.";
		public override string[] CallAliases => new string[] { "island seed <seed>", "generate island <seed>", "island with seed <seed>", "island of seed <seed>" };

		public override async Task Execute(CommandContext context) {
			string passedSeed = context.Args["seed"];
			bool isInt = int.TryParse(passedSeed, out int seed);
			if (!isInt)
				seed = Island.GetNameSeed(passedSeed);

			NoiseMap map = Island.GenerateMap(seed, 600, 600, 2, 2);
			string filename = Island.RenderAndSave(map);

			await context.ReplyEmbed("Island geneerated", $"An island has been generated with seed {seed.ToString().Bold()}.", imagePath: filename);
			File.Delete(filename);
		}
	}
}
