﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheAuthority.Game {
	class NationExistsException : Exception { }
	class NoNationExistsException : Exception { }
	class CitizenExistsException : Exception { }
	class NoCitizenException : Exception { }
}
