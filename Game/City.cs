﻿using MongoDB.Bson.Serialization.Attributes;
using SixLabors.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheAuthority.Game
{
	class GeographicLocation {
		[BsonElement("x")]
		public int X;

		[BsonElement("y")]
		public int Y;

		[BsonIgnore]
		public Point Coordinates => new Point(X, Y);

		public GeographicLocation(int x, int y) {
			X = x;
			Y = y;
		}
	}

    class City : NationObject {
		[BsonElement("name")]
		public string Name;

		[BsonElement("coords")]
		public GeographicLocation Location;

		[BsonElement("pop")]
		public int Population;
    }
}
