﻿using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;
using SharpNoise.Utilities.Imaging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.Text;
using TheAuthority.Discord;

namespace TheAuthority.Game
{
	class IslandMask : Module {
		public const double DefaultRadius = 1D;

		public double Radius { get; set; } = DefaultRadius;

		public IslandMask() : base(0) {

		}

		public override double GetValue(double x, double y, double z) {
			double value = Math.Max(-1, Math.Sqrt(x * x + y * y + z * z) * 2 - 1);
			return -value;
			//double value = Math.Max(0, 1 - Math.Sqrt(x * x + y * y + z * z));
			//return value;
		}
	}

    class Island
    {
		/*public static int GetNameSeed(string name) {
			int seedVal = 0;
			bool lastSub = false;
			foreach (char ch in name) {
				lastSub = !lastSub;
				if (lastSub)
					seedVal += Convert.ToByte(ch);
				else
					seedVal -= Convert.ToByte(ch);
			}
			seedVal = name.Length % 2 == 0 ? seedVal : -seedVal;
			return seedVal;
		}*/
		public static int GetNameSeed(string name) => name.GetStableHashCode();

		public static NoiseMap GenerateMap(int seed, int width, int height, int pollX, int pollY) {
			Module module = new Add {
				Source0 = new Perlin { Seed = seed, Quality = NoiseQuality.Best },
				Source1 = new IslandMask { Radius = width / 2d }
			};

			//Module module = new Multiply {
			//	Source0 = new Perlin { Seed = seed, Quality = NoiseQuality.Best },
			//	Source1 = new IslandMask { Radius = width / 2d }
			//};

			NoiseMap map = new NoiseMap();
			PlaneNoiseMapBuilder mapBuilder = new PlaneNoiseMapBuilder {
				DestNoiseMap = map,
				SourceModule = module
			};

			mapBuilder.SetDestSize(width, height);
			mapBuilder.SetBounds(-pollX / 2d, pollX / 2d, -pollY / 2d, pollY / 2d);
			mapBuilder.Build();

			return map;
		}

		public static string RenderAndSave(NoiseMap map) {
			Image<Rgba32> image = RenderNoiseMap(map);
			string file = SaveImage(image, $"heightmap-{map.Width}-{map.Height}");
			image.Dispose();
			return file;
		}

		public static string SaveImage(Image<Rgba32> image, string name) {
			image.Save($"{name}.png", new PngEncoder());
			return $"{name}.png";
		}

		public static Image<Rgba32> RenderNoiseMap(NoiseMap map) {
			SharpNoise.Utilities.Imaging.Image image = new SharpNoise.Utilities.Imaging.Image();
			ImageRenderer renderer = new ImageRenderer {
				SourceNoiseMap = map,
				DestinationImage = image
			};

			
			// procedural water
			Color bottomColor = new Color(0, 60, 160, 255);
			Color topColor = new Color(30, 80, 220, 255);

			float waterShades = 3;

			for (int i = 0; i < waterShades; i++) {
				Color myColor = Color.LinearInterpColor(bottomColor, topColor, i / waterShades);
				renderer.AddGradientPoint(-1 + (i / waterShades) + 0.005, myColor);
				renderer.AddGradientPoint(-1 + ((i + 1) / waterShades), myColor);
			}

			// sand
			renderer.AddGradientPoint(0.005, new Color(226, 213, 108, 255));
			renderer.AddGradientPoint(0.1, new Color(226, 213, 108, 255));

			// grass
			renderer.AddGradientPoint(0.105, new Color(93, 175, 98, 255));
			renderer.AddGradientPoint(0.3, new Color(93, 175, 98, 255));

			// darker grass
			renderer.AddGradientPoint(0.305, new Color(72, 132, 76, 255));
			renderer.AddGradientPoint(0.5, new Color(72, 132, 76, 255));

			// darkest grass
			renderer.AddGradientPoint(0.505, new Color(65, 109, 68, 255));
			renderer.AddGradientPoint(0.7, new Color(65, 109, 68, 255));

			// low mountain
			renderer.AddGradientPoint(0.705, new Color(170, 170, 170, 255));
			renderer.AddGradientPoint(0.85, new Color(170, 170, 170, 255));

			// high mountain
			renderer.AddGradientPoint(0.855, new Color(206, 206, 206, 255));
			renderer.AddGradientPoint(0.95, new Color(206, 206, 206, 255));

			// highest mountain
			renderer.AddGradientPoint(0.955, new Color(230, 230, 230, 255));
			renderer.AddGradientPoint(1, new Color(230, 230, 230, 255));

			//renderer.BuildGrayscaleGradient();
			renderer.Render();

			string filename = $"heightmap.png";

			Image<Rgba32> img = new Image<Rgba32>(map.Width, map.Height);

			for (int y = 0; y < map.Height; y++) {
				for (int x = 0; x < map.Width; x++) {
					Color color = image.GetValue(x, y);
					img[x, y] = new Rgba32(color.Red, color.Green, color.Blue);
				}
			}

			return img;
		}
	}
}
