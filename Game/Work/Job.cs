﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAuthority.Game.Work
{
	class CompanyJob {
		public Job Job;
		public Company Company;

		public CompanyJob(Company company, Job job) {
			Company = company;
			Job = job;
		}
	}

    class Job
    {
		[BsonElement("name")]
		public string Name;

		[BsonElement("pay")]
		public float PayScale = 0; // A static payment factor. If one job has a pay scale of 2 and another of 1, then the first job will get 66% of profit and the second will get 33% because 2 + 1 = 3, and 2 / 3 is .66 and 1/3 is .33

		[BsonElement("occupant")]
		public ulong? OccupantId;

		[BsonElement("occupants")]
		public List<ulong> Occupants;

		public Job(string name, double pay) {
			Name = name;
			PayScale = (float)pay;
		}
    }

	class SingleJob : Job {
		public Citizen GetOccupant(Nation nation) {
			if (!OccupantId.HasValue)
				return null;

			return nation.Citizens.FirstOrDefault(c => c.DiscordId == OccupantId.Value);
		}

		public SingleJob(string name, double pay) : base(name, pay) { }
	}

	class ManyJob : Job {
		public List<Citizen> GetOccupants(Nation nation) => nation.Citizens.Where(c => Occupants.Contains(c.DiscordId)).ToList();

		public ManyJob(string name, double pay) : base(name, pay) { }
	}
}
