﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAuthority.Game.Work
{
	class Jobs : List<Job> {
		public Job Get(string name) => this.FirstOrDefault(j => j.Name == name);
	}

    class Company {
		[BsonElement("name")]
		public string Name;

		[BsonElement("jobs")]
		public Jobs Jobs = new Jobs();

		public uint GetJobPay(uint profit, Job job) {
			float cumulativeScale = Jobs.Aggregate(0f, (p, c) => p += c.PayScale);
			return (uint)Math.Floor(job.PayScale / cumulativeScale);
		}

		/// <summary>
		/// Forcibly gives the Citizen the specified Job. If the job is a SingleJob, then it overrides the old occupant.
		/// </summary>
		/// <param name="citizen">The citizen to give the Job to.</param>
		/// <param name="job">The job to be given.</param>
		/// <returns></returns>
		public Job GiveCitizenJob(Citizen citizen, Job job) {
			if (job is SingleJob)
				(job as SingleJob).OccupantId = citizen.DiscordId;
			else if (job is ManyJob)
				(job as ManyJob).Occupants.Add(citizen.DiscordId);

			return job;
		}
		/// <summary>
		/// Forcibly gives the Citizen the specified job name. A lookup finds the job and appropriates it like GiveCitizenJob(Citizen, Job) does.
		/// </summary>
		/// <param name="citizen">The citizen to give the Job to.</param>
		/// <param name="jobName">The job to be given.</param>
		/// <returns></returns>
		public Job GiveCitizenJob(Citizen citizen, string jobName) => GiveCitizenJob(citizen, Jobs.Get(jobName));


		public Job RemoveCitizenJob(Citizen citizen, Job job) {
			if (job is SingleJob)
				(job as SingleJob).OccupantId = null;
			else if (job is ManyJob)
				(job as ManyJob).Occupants.Remove(citizen.DiscordId);

			return job;
		}
		public Job RemoveCitizenJob(Citizen citizen, string jobName) => RemoveCitizenJob(citizen, Jobs.Get(jobName));
	}
}
